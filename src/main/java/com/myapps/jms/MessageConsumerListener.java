package com.myapps.jms;

import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class MessageConsumerListener implements MessageListener {

	Logger logger = Logger.getLogger(MessageConsumerListener.class
			.getSimpleName());

	public void onMessage(Message message) {
		try {
			logger.info("Received message: "
					+ ((TextMessage) message).getText());
		} catch (JMSException e) {
			logger.severe(e.getMessage());
		}
	}

}
